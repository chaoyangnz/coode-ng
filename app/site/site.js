angular.module('coode.site', []).
controller('SiteController', ['$scope', '$modal', '$location', function ($scope, $modal, $location) {
    $scope.search = function($event) {
        $location.path('/?q')
    }
    $scope.notif = {
        templateUrl: 'site/notif.html'
    }

    $scope.sos = {
        open: function () {
            var sos_modal = $modal.open({
                animation: true,
                templateUrl: 'site/sos.html',
                controller: 'SOSController',
                size: 'lg',
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }
            });

            sos_modal.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                console.info('Modal dismissed at: ' + new Date());
            });
        }
    }

}]).

controller('SOSController', ['$scope', '$modalInstance', function($scope, $modalInstance) {
    $scope.submit = function() {

    }

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    }
}]).
config(['$httpProvider', function ($httpProvider) {
    // Enable CORS
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
    //$httpProvider.defaults.withCredentials = true;
    $httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";

}]).

factory('backend', function($http, $q) {
    var db = "http://localhost:8000/"

    function insert(url, data) {
        var deferred = $q.defer();
        var request = db + url

        $http.get(request, {}).success(function (res) {

            //resolve promise
            deferred.resolve(res.meta);
        });

        deferred.promise.then(function(meta) {
            var total_count = meta.total_count
            data.id = (total_count+1).toString()
            $http.post(request, data)
            deferred.resolve()
        })

        return deferred.promise
    }

    function query(url) {
        //promise to return
        var deferred = $q.defer();

        var request = db + url

        $http.get(request, {}).success(function (res) {

            //resolve promise
            deferred.resolve(res.objects);
        });

        return deferred.promise
    }

    function query_one(url) {
        var deferred = $q.defer();

        var request = db + url

        $http.get(request, {}).success(function (res) {

            //resolve promise
            deferred.resolve(res.objects.length > 0 ? res.objects[0] : {});
        });

        return deferred.promise
    }


    return {
        query_one: query_one,
        query: query,
        insert: insert
    }
})

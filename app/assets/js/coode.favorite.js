$(function() {
	var deletefavid;
	$('.delete').click(function() {
//		console.log($(this).attr('favid'));
		$('#delete-modal-body strong').text($(this).attr('favname'));
		deletefavid = $(this).attr('favid');
	})
	
	$('#deletefav').click(function(){
			$.ajax({
			url : app.contextPath + '/deletefav.json',
			dataType : 'json',
			type : 'PostContent',
			data : { favoriteid : deletefavid }
		}).done(function(data) {
			location.reload();
		})
	})
	
	var favoriteid;
	$('.rename').click(function(){
		$('#rename-modal-body input').val($(this).attr('favname'));
		favoriteid=$(this).attr('favid');
//		console.log($(this).attr('favname'));
	})
	
	$('#renamefav').click(function(){
//		console.log($('#rename-modal-body input').val());
		$.ajax({
			url: app.contextPath + '/updatefavname.json',
			dataType:'json',
			type:'PostContent',
			data:{
				favoriteid: favoriteid,
				favoritename: $('#rename-modal-body input').val()
			}
		}).done(function(data) {
			location.reload();
//			console.log(data);
		})
	})
})
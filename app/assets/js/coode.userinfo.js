$(function() {
	//信息页面-修改页面切换
	$('#edit').click(function() {
		$('#userinfo-show').addClass('hide');
		$('#userinfo-edit').removeClass('hide');
		$('#photo').addClass('hide');
		$('#photo-edit').removeClass('hide');
	})
	//保存修改
	$('#save-edit').click(function() {
		$('#userinfo-show').removeClass('hide');
		$('#userinfo-edit').addClass('hide');
		$('#photo').removeClass('hide');
		$('#photo-edit').addClass('hide');
//		console.log( $('#nickname').val());
//		console.log( $('#dept-select').val());
//		console.log( $('#dept-visible').prop('checked'));
//		console.log( $('#position-edit').val());
//		console.log( $('#experience-edit').val());
		$.ajax({
			url : app.contextPath + '/saveuserinfo.json',
			dataType : 'json',
			type : 'PostContent',
			data : {
				nickname : $('#nickname').val(),
				dept:$('#dept-select').val(),
				deptvisible:$('#dept-visible').prop('checked'),
				position:$('#position-edit').val(),
				projexp:$('#experience-edit').val()
			}
		}).done(function(data){
			location.reload();
		})
	})
//	发起、回应、草稿
	$('.mynodes').click(function(){
		$('.mynodes').parents().removeClass('active');
		$(this).parent().addClass('active');
//		console.log($(this).attr('userid'));
//		console.log($(this).attr('id'));
		$.ajax({
			url: app.contextPath + '/showmypost',
			dataType:'html',
			data:{
				userid:$(this).attr('userid'),
				nodetype:$(this).attr('id')				
			}
		}).done(function(data){
//			console.log(data);
			$('#nodelist').html(data);
		})
	})
	//默认点击发起
	$('#mypost').trigger('click');
	
	var type = window.location.hash;
//	console.log(type == '#mydraft');
	if(type == '#mydraft'){
//		console.log(111);
		$('#mydraft').trigger('click');
	}
	
    //上传头像
	window.onload = function() {  
	    init();  //初始化  
	}  
	
	//初始化  
	function init() {      
	    //初始化图片上传  
	    var btnImg = document.getElementById("btnUploadImg");  
	    var img = document.getElementById("imgShow");  
	    var hidImgName = document.getElementById("hidImgName");  
	    g_AjxUploadImg(btnImg, img, hidImgName);  
	} 

	var g_AjxTempDir = "/file/temp/";  
	//图片上传  
	function g_AjxUploadImg(btn, img, hidPut) {  
//		console.log(hidPut);
//		console.log(hidPut.parentNode);
	    var button = btn, interval;  
	    new AjaxUpload(button, {  
	        action: 'ProcessRequest',  
	        data: {},  
	        name: 'myfile',  
	        onSubmit: function(file, ext) {  
	            if (!(ext && /^(jpg|JPG|png|PNG|gif|GIF)$/.test(ext))) {  
	                alert("您上传的图片格式不对，请重新选择！");  
	                return false;  
	            }  
	        },  
	        onComplete: function(file, response) { 
//	        	location.reload();
//		console.log(file);	        	
	        	$.ajax({
	        		url: app.contextPath + '/myuserinfo.json'
	        	}).done(function(data){
	        		var userJson = jQuery.parseJSON(data); 
//	        		console.log(userJson.USERINFO.HEAD_ID);
	        		$('#photo').html("<img src=\"/coode/assets/photo/"+userJson.USERINFO.HEAD_ID+"\" alt=\"头像\">");
	        		$('#photobackground').html("<img src=\"/coode/assets/photo/"+userJson.USERINFO.HEAD_ID+"\" alt=\"头像\">");
	        		$('#photo70').attr("src","/coode/assets/photo/"+userJson.USERINFO.HEAD_ID);
	        		$('#photo180').attr("src","/coode/assets/photo/"+userJson.USERINFO.HEAD_ID);
	        	})
	        }  
	    });  
	} 

	//删除图片  
	$('#btnDeleteImg').click(function(){
		$.ajax({
			url: app.contextPath + '/deletephoto.json'
		}).done(function(){
			$('#photo').html("<img src=\"/coode/assets/images/avatar-180x180.png\">");
    		$('#photobackground').html("<img src=\"/coode/assets/images/avatar-180x180.png\">");
    		$('#photo70').attr("src","/coode/assets/images/avatar-180x180.png");
    		$('#photo180').attr("src","/coode/assets/images/avatar-180x180.png");
		})
	})
})
'use strict';

// Declare app level module which depends on views, and components
angular.module('coode', [
  'ngRoute',
  'ui.utils',
  'ui.bootstrap',
  'ui.tinymce',
  'coode.site',
  'coode.node',
  'coode.view2',
  'coode.version',
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/'});
}]);

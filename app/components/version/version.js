'use strict';

angular.module('coode.version', [
  'coode.version.interpolate-filter',
  'coode.version.version-directive'
])

.value('version', '0.1');

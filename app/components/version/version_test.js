'use strict';

describe('coode.version module', function() {
  beforeEach(module('coode.version'));

  describe('version service', function() {
    it('should return current version', inject(function(version) {
      expect(version).toEqual('0.1');
    }));
  });
});

'use strict';

angular.module('coode.node', []).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.
      when('/', {
        templateUrl: 'node/list.html',
        controller: 'NodeListController'
      }).
      when('/nodes/new', {
        templateUrl: 'node/new.html',
        controller: 'NodeNewController'
      }).
      when('/nodes/:id', {
        templateUrl: 'node/show.html',
        controller: 'NodeShowController'
      }).
      otherwise({redirectTo: '/'});
}]).

controller('NodeListController', ['$scope', '$http', function($scope, $http) {
    //var q = $scope.q
    //
    //backend.query('nodes').then(function(data) {
    //    $scope.nodes = data
    //})
    $http.get("http://localhost:8080/nodes").success(function(nodes) {
        $scope.nodes = nodes
    })
}]).

controller('NodeNewController', ['$scope', '$http', '$location', function($scope, $http, $location) {

    // initialize and data
    $scope.contentTinymceOptions = {

    }

    $scope.node = {
        //title: 'A new article',
        //content: 'placeholder',
        category: 'question'
    }

    // event handlers
    $scope.submit_draft = function (node) {

    }

    $scope.submit = function() {
        //backend.insert("nodes", $scope.node).then(function() {
        //    //redirect
        //    $location.path('/?refresh')
        //})
        $http.post("http://localhost:8080/nodes", $scope.node).success(function() {
            $location.path('/?refresh')
        })
    }

    $scope.reset = function() {
        $scope.node = {}
    }
}]).

controller('NodeShowController', ['$scope', '$routeParams', '$http', function($scope, $routeParams, $http) {
    //$scope.id = $routeParams.id
    //backend.query_one('nodes?query={"id":"' + $routeParams.id + '"}').then(function(data) {
    //    $scope.node = data
    //})
    $http.get("http://localhost:8080/nodes/" + $routeParams.id).success(function(node) {
        $scope.node = node
    })
}]);
